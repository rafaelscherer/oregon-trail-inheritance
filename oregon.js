////////////
//Traveler//
////////////
function Traveler(name, food, isHealthy) {
    this.name = name;
    this.food = 1;
    this.isHealthy = true;    
}

Traveler.prototype.hunt = function() {
    this.food = this.food + 2;    
}

Traveler.prototype.eat = function() {
    if (this.food <= 0) {
        this.isHealthy = false;        
    } 
    else {
        this.food = this.food - 1;
    }
}

//////////
//Doctor//
//////////
function Doctor(name) {
    Traveler.call(this, name);
}

Doctor.prototype = Object.create(Traveler.prototype);
Doctor.prototype.constructor = Doctor;
Doctor.prototype.heal = function(Traveler) {
    Traveler.isHealthy = true;
}


//////////
//Hunter//
//////////
function Hunter(name) {
    Traveler.call(this, name);
    this.food = 2;
}

Hunter.prototype = Object.create(Traveler.prototype);
Hunter.prototype.constructor = Hunter;
Hunter.prototype.hunt = function() {
    this.food = this.food + 5;
}

Hunter.prototype.eat = function() {     
    if (this.food > 1) {
        this.food = this.food - 2;
    } 
    else if (this.food = 1) {
        this.food = this.food - 1;
        this.isHealthy = false;        
    } 
    else if (this.food = 0) {
        this.isHealthy = false;
    }
}

Hunter.prototype.giveFood = function(Traveler, numOfFoodUnits) {
    if (this.food >= numOfFoodUnits) {
        Traveler.food = Traveler.food + numOfFoodUnits;
        this.food = this.food - numOfFoodUnits;              
    } 
    
}


/////////
//Wagon//
/////////
function Wagon(capacity, passageiros) {
    this.capacity = capacity;
    this.passageiros = [];    
}

Wagon.prototype.getAvailableSeatCount = function() {
    return this.capacity - this.passageiros.length; 
}

Wagon.prototype.join = function(Traveler) {
    if (this.getAvailableSeatCount() > 0) {
        this.passageiros.push(Traveler);
    }
}

Wagon.prototype.shouldQuarantine = function() {
    return this.passageiros.some(passageiros => {
        if (passageiros.isHealthy === false) {
        return true;
        }
    })
}

Wagon.prototype.totalFood = function () {
    let total = 0;
    this.passageiros.forEach(Travelers => {
        total = total + Travelers.food;
    })
    return total;
}

// Cria uma carroça que comporta 4 pessoas
let wagon = new Wagon(4);
// Cria cinco viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let drsmith = new Doctor('Dr. Smith');
let sarahunter = new Hunter('Sara');
let maude = new Traveler('Maude');

console.log(`#1: There should be 4 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

wagon.join(henrietta);
console.log(`#2: There should be 3 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

wagon.join(juan);
wagon.join(drsmith);
wagon.join(sarahunter);

wagon.join(maude); // Não tem espaço para ela!
console.log(`#3: There should be 0 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

console.log(`#4: There should be 5 total food. Actual: ${wagon.totalFood()}`);

sarahunter.hunt(); // pega mais 5 comidas
drsmith.hunt();

console.log(`#5: There should be 12 total food. Actual: ${wagon.totalFood()}`);

henrietta.eat();
sarahunter.eat();
drsmith.eat();
juan.eat();
juan.eat(); // juan agora está doente (sick)

console.log(`#6: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#7: There should be 7 total food. Actual: ${wagon.totalFood()}`);

drsmith.heal(juan);
console.log(`#8: Quarantine should be false. Actual: ${wagon.shouldQuarantine()}`);

sarahunter.giveFood(juan, 4);
sarahunter.eat(); // Ela só tem um, então ela come e fica doente

console.log(`#9: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#10: There should be 6 total food. Actual: ${wagon.totalFood()}`);
